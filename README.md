# Front test

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

en el directorio del proyecto, ejecuta para instalar dependencias:

### `npm install`

despues para su ejecucion local, y podras revisarlo:

### `npm start`

Se ejecutara en development mode.\
Abre [http://localhost:3000](http://localhost:3000) para verlo en tu navegador.

para generar una imagen docker ejecuta.

### `docker build -t n5_front .`

una vez generada la imagen, para que se ejecute en la misma red de elasticsearch y de la api (previamente ejecutada en docker).

### `docker run -d --net elastic -p 8081:80 --name n5-container-2 n5_front`