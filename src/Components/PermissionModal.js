import React, { useState } from 'react';
import {
  Modal,
  Backdrop,
  Fade,
  TextField,
  Button,
  Grid,
  Paper,
  Select,
  MenuItem,
  Snackbar,
} from '@mui/material';
import MuiAlert from '@mui/material/Alert';

import { GetPermissionsService } from '../Services/PermissionService';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function PermissionModal({ open, onClose, permisionTypes }) {
  const [nombre, setNombre] = useState('');
  const [apellido, setApellido] = useState('');
  const [tipoPermiso, setTipoPermiso] = useState('');
  const [fecha, setFecha] = useState('');
  const [errorAlertOpen, setErrorAlertOpen] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {

        if (nombre != '' && apellido != '' && tipoPermiso != '' && fecha != '') {

            const resp = await GetPermissionsService.createPermission({ nombreEmpleado: nombre, apellidoEmpleado: apellido, tipoPermiso: parseInt(tipoPermiso), fechaPermiso: fecha });

            if (resp.status == 'ok') {
              setNombre('');
              setApellido('');
              setTipoPermiso('');      
              setFecha('');
              onClose();
      
            } else {      
                setErrorAlertOpen(true);
            }

        } else {

        }

      

    } catch (error) {
      console.error('Error creating permission:', error);
      setErrorAlertOpen(true);
    }
  };

  function handleAlertClose() {
    setErrorAlertOpen(false);
  };

  const modalStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      closeAfterTransition
      style={modalStyle}
    >
      <Fade in={open}>
        <Paper elevation={3} style={{ padding: 16, width: 300 }}>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  label="Nombre"
                  fullWidth
                  value={nombre}
                  onChange={(e) => setNombre(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Apellido"
                  fullWidth
                  value={apellido}
                  onChange={(e) => setApellido(e.target.value)}
                />
              </Grid>              
              <Grid item xs={12}>
                <TextField
                  label="Fecha"
                  type='date'
                  fullWidth
                  value={fecha}
                  onChange={(e) => setFecha(e.target.value)}
                />
              </Grid>              
              <Grid item xs={12}>
              <Select
                label="Tipo de Permiso"
                fullWidth
                value={tipoPermiso}
                onChange={(e) => setTipoPermiso(e.target.value)}
                >
                {permisionTypes.map(type => (
                    <MenuItem key={type.id} value={type.id}>
                        {type.descripcion}
                    </MenuItem>
                ))}
                </Select>
              </Grid>
              <Grid item xs={12}>
                <Button type="submit" variant="contained" color="primary">
                  Crear Permiso
                </Button>
                <Button type="button" variant="contained" color="warning" onClick={onClose}>
                  Cerrar
                </Button>
              </Grid>
            </Grid>
          </form>
            <Snackbar open={errorAlertOpen} autoHideDuration={6000} onClose={handleAlertClose}>
                <Alert onClose={handleAlertClose} severity="error">
                    Hemos tenido un error, intente mas adelante.
                </Alert>
            </Snackbar>
        </Paper>
      </Fade>
    </Modal>
  );
}

export default PermissionModal;