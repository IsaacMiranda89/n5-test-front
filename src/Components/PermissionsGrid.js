import React, { useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'nombreEmpleado', headerName: 'Nombre', width: 150 },
  { field: 'apellidoEmpleado', headerName: 'Apellido', width: 150 },
  { field: 'tipoPermiso', headerName: 'Tipo', width: 100 },
  { field: 'fechaPermiso', headerName: 'Fecha Permiso', width: 300 },
];

function PermissionsGrid(props) {

    const { rows } = props;

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5, 10, 20]}
        checkboxSelection
      />
    </div>
  );
}

export default PermissionsGrid;