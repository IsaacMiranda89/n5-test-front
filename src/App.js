import logo from './logo.svg';
import './App.css';

import { GetPermissionTypeService, GetPermissionsService } from './Services/PermissionService';

import Header from './Components/Header';
import PermissionsGrid from './Components/PermissionsGrid';
import PermissionModal from './Components/PermissionModal';

import { Button } from '@mui/material';

import React, { useState, useEffect } from 'react';

function App() {

  const [permissionTypes, setPermissionTypes] = useState([]);
  const [permissions, setPermissions] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    fetchPermissionTypes();
    fetchPermissions();
  }, []);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    fetchPermissions();
  };
  
  async function fetchPermissionTypes() {
    try {
      const types = await GetPermissionTypeService.getPermissionTypes()
      setPermissionTypes(types);
    } catch (error) {
      console.error('Error fetching permission types:', error);
    }
  }

  async function fetchPermissions() {
    try {
      const permitions = await GetPermissionsService.getPermission()
      setPermissions(permitions);
    } catch (error) {
      console.error('Error fetching permissions:', error);
    }
  }

  return (
    <div className="App">
      <Header />
      <div style={{padding: "1em"}}>
        <Button variant="contained" color="primary" onClick={openModal}>
          Nuevo Permiso
        </Button>
      </div>
      <PermissionModal open={isModalOpen} onClose={closeModal} permisionTypes={permissionTypes} />
      <PermissionsGrid rows={permissions} />
    </div>
  );
}

export default App;
