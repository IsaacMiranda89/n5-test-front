import axios from 'axios';

const API_BASE_URL = process.env.REACT_APP_API_URL;

const GetPermissionTypeService = {
  getPermissionTypes: async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/PermissionTypes`);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};

const GetPermissionsService = {
    getPermission: async () => {
      try {
        const response = await axios.get(`${API_BASE_URL}/Permissions`);
        return response.data;
      } catch (error) {
        throw error;
      }
    },

    createPermission: async (permissionData) => {
        try {
          const response = await axios.post(`${API_BASE_URL}/Permissions`, permissionData);
          return response.data;
        } catch (error) {
          throw error;
        }
      },
  };

export { GetPermissionTypeService, GetPermissionsService } ;